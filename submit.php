<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
<label for="name1">Name1 : </label> 
<input type="text" name="name1">
<br>
<label for="healthid1">Healthid1 : </label> 
<input type="text" name="healthid1">
<br>
<label for="name2">Name2 : </label> 
<input type="text" name="name2">
<br>
<label for="healthid2">Healthid2 : </label> 
<input type="text" name="healthid2">
<br>
<label for="name3">Name3 : </label> 
<input type="text" name="name3">
<br>
<label for="healthid3">Healthid3 : </label> 
<input type="text" name="healthid3">
<button type="submit" name="submit" value="SUBMIT">Submit</button>
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
   $message='';
   $myFieldNames = ['name1','healthid1','name2','healthid2','name3','healthid3'];
    foreach ($myFieldNames as $name) {
        if (!empty($_POST[$name])) 
        {
         if(strpos($name, 'name') !== false){
            if(strlen($_POST[$name]) < 3){
               // $message .=  $name . " is minimum 3 Charaters required!";
               echo  $name . " is minimum 3 Charaters required!";
               die;
            }
         }elseif(strpos($name, 'health') !== false){
            if(!preg_match("/^[0-9]{5}$/", $_POST[$name])){
               echo  $name . " is must be a 5-digit number.";
               die;
            }
         }
        }else{
           echo "Please fill $name the fields";
           die;
        }
    }
    if($_POST['submit']) {
      echo "SUCCESS";
      die;
    }
}
?>